#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#-- Script to read, process, and plot the soil data recorded every 15-minutes (soil VWC) from the S39 glasshouse
#-- Figure S1
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

dat.fast <- data.frame(data.table::fread("Data/Glasshouse_DRAKE_EUTE_THERMAL-NICHE/data/GHS39_GREAT_MAIN_MET-VWC_20160114-20160302_L0.csv"))
dat.fast$DateTime <- as.POSIXct(dat.fast$DateTime,format="%Y-%m-%d %T",tz="UTC")
dat.fast$Date <- as.Date(dat.fast$Date)
# dat.fast<-subset(dat.fast,dat.fast$VWC>0)
# #-----------------------------------------------------------------------------------------
# #-----------------------------------------------------------------------------------------
# #- create daily averages

dat.fast.day<-summaryBy(VWC~Date+Room+W_treatment+Code,FUN=c(mean),data=dat.fast,keep.names=T,na.rm=T)
dat.fast.day$W_treatment<-factor(dat.fast.day$W_treatment)



dat.fast.day.1<-summaryBy(VWC~Date+Room+W_treatment,FUN=c(mean,std.error),data=dat.fast.day,na.rm=T)

key <- data.frame(Room=1:6,Tair= c(18,21.5,25,28.5,32,35.5)) 
dat.fast.day.1 <- merge(dat.fast.day.1,key,by="Room")

# dat.fast.day$Date <- as.Date(dat.fast.day$Date)
dat.fast.day.1 <- as.data.frame(dat.fast.day.1)
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#- plot VWC for each room  during the experiment

palette(rev(brewer.pal(6,"Spectral")))
COL=palette()[c(6,1)]
pdf(file="output/Figure1.pdf",width=5,height=8)
par(mfrow=c(6,1),mar=c(0.5,0,0,0),oma=c(4,7,1,1))


dat.vwc.l <- split(dat.fast.day.1,dat.fast.day.1$Room)

# plot first panel
# thi is just to put legend

plotBy(VWC.mean~Date|W_treatment,data=dat.vwc.l[[1]],type="l",lwd=2,col=COL,
       legend=F,ylim=c(0,0.3),las=1)

adderrorbars(x=dat.vwc.l[[1]]$Date,y=dat.vwc.l[[1]]$VWC.mean,SE=dat.vwc.l[[1]]$VWC.std.error,
             direction="updown",col=COL[dat.vwc.l[[1]]$W_treatment])

points(VWC.mean~Date,data=dat.vwc.l[[1]],add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[dat.vwc.l[[1]]$W_treatment])       

# rect(xleft=as.Date("2016-02-16"),ybottom=0.01,xright=as.Date("2016-02-24"),ytop=0.02,col=alpha("lightgrey",0.5))      
rug(as.Date("2016-02-02"),lwd=3,line=-0.5)
rug(as.Date("2016-02-24"),lwd=3,line=-0.5)

# legend("topright",letters[i],bty="n",xpd=NA,cex=1.3)
legend("topright",paste("(",letters[1],")",sep=""),bty="n",cex=1.5,text.font=2)

mylabel_1 = bquote(T[growth] == .(format(dat.vwc.l[[1]]$Tair[[1]], digits = 3))~degree*C)

legend("top",legend=mylabel_1,bty="n",cex=1.3,text.font=2)

legend("bottomleft",c(expression(W[incr]),expression(W[const])),pt.bg=COL[c(2,1)],cex=1.3,title="Treatment",bty="n",pch=21,pt.cex=1.1)

abline(h=0.175,lty=1,lwd=2,col=alpha("lightgray",0.5))
#-----
# plot rest

for (i in 2:length(dat.vwc.l)){
  
  toplot <- dat.vwc.l[[i]]
  
  plotBy(VWC.mean~Date|W_treatment,data=toplot,type="l",lwd=2,col=COL,
         legend=F,ylim=c(0,0.3),las=1)
  
  adderrorbars(x=toplot$Date,y=toplot$VWC.mean,SE=toplot$VWC.std.error,
               direction="updown",col=COL[toplot$W_treatment])
  
  points(VWC.mean~Date,data=toplot,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[toplot$W_treatment])       
  
  # rect(xleft=as.Date("2016-01-29"),ybottom=-0.2,xright=as.Date("2016-02-09"),ytop=0.42,col=alpha("lightgrey",0.5))      
  rug(as.Date("2016-02-02"),lwd=3,line=-0.5)
  rug(as.Date("2016-02-24"),lwd=3,line=-0.5)
  
  # legend("topright",letters[i],bty="n",xpd=NA,cex=1.3)
  legend("topright",paste("(",letters[i],")",sep=""),bty="n",cex=1.5,text.font=2)
  
  mylabel_1 = bquote(T[growth] == .(format(toplot$Tair[[1]], digits = 3))~degree*C)
  
  legend("top",legend=mylabel_1,bty="n",cex=1.3,text.font=2)
  abline(h=0.175,lty=1,lwd=2,col=alpha("lightgray",0.5))
  # axis.Date(side=1,at=seq.Date(from=min(dat.fast.day$Date),to=max(dat.fast.day$Date),by="day"),labels=F)
}

title(ylab=expression(theta~(m^3~m^-3)),xlab="Date",
      outer=T,cex.lab=2.5)
axis.Date(side=1,at=seq.Date(from=min(dat.fast.day$Date),to=max(dat.fast.day$Date)+5,by="week"),labels=T)

# legend("bottomleft",c("Dry","Wet"),fill=COL,cex=0.8,title="Treatment",bty="n")

dev.off()
