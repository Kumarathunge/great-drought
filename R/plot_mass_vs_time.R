
# plot time course of plant growth

path="Data/Glasshouse_DRAKE_EUTE_THERMAL-NICHE/data"
  
  #-----------------------------------------------------------------------------------------
  #- get the size data, estimate mass
  
  hddata <- getSize(path=path) 
  hddata$totmass <- returnMassFromAllom(d2hdat=hddata$d2h,plotson=F,droughtdat = T) #- predict mass from d2h allometry
  hddata <- hddata[with(hddata,order(W_treatment,Code,Date)),]
  hddata <- hddata[complete.cases(hddata),]                  # remove missing data
  # hddata <- subset(hddata,W_treatment=="w")                  # remove drought data
  hddata$Code <- factor(hddata$Code)
  #-----------------------------------------------------------------------------------------
  
  
  
  #-----------------------------------------------------------------------------------------
  #- calculate RGR and AGR based on the estimated total mass
  hddata <- hddata[with(hddata,order(Code,Date)),]
  
  hddata.l <- split(hddata,hddata$Code)
  for(i in 1:length(hddata.l)){
    #print(i)
    crap <- hddata.l[[i]]
    hddata.l[[i]]$RGR <- c(NA,diff(log(hddata.l[[i]]$totmass)))/c(NA,diff(hddata.l[[i]]$Date)) # g g-1 day-1
    hddata.l[[i]]$AGR <- c(NA,diff(hddata.l[[i]]$totmass))/c(NA,diff(hddata.l[[i]]$Date))      # g day-1
    
  }
  hddata2 <- do.call(rbind,hddata.l)
  
  
  #- pull out the most important agr and rgr estimates (during our growth interval from Jan 28th to Feb 8th)
  rgrinterval1 <- subset(hddata2,Date %in% as.Date(c("2016-01-28","2016-02-08")))
  rgrinterval <- rgrinterval1[complete.cases(rgrinterval1),] # remove missing data
  #-----------------------------------------------------------------------------------------
  
  # with(rgrinterval,plot(Tair,totmass,col=W_treatment))
  
  prov_b<-subset(hddata,hddata$Prov=="B")
  
  # with(subset(prov_b,prov_b$Room==6),plot(Date,totmass,col=W_treatment))
  
  growth_dat<-summaryBy(totmass+Height+diam+Tair~Date+Room+W_treatment,FUN=c(mean,standard.error),data=prov_b)
  #------------------------------------------------------------------------------------------------------------
  #------------------------------------------------------------------------------------------------------------
  #-----------------------------------------------------------------------------------------
  #-----------------------------------------------------------------------------------------
  #- plot VWC for each room  during the experiment
  
  
  
  growth_mean <- split(growth_dat,growth_dat$Room)
  
  palette(rev(brewer.pal(6,"Spectral")))
  COL=palette()[c(1,6)]
  pdf(file="output/FigureS4.pdf",width=5,height=8)
  par(mfrow=c(6,1),mar=c(0.5,0,0,0),oma=c(4,7,1,1),cex.axix=1.5)
  
  
  # windows(40,70);par(mfrow=c(6,1),mar=c(0,0,0,0),oma=c(6,7,1,4))
  for (i in 1:length(growth_mean)){
    
    toplot <- growth_mean[[i]]
    

    
     plotBy(totmass.mean~Date|W_treatment,data=toplot,type="l",lwd=2,col=COL,
            legend=F,ylim=c(0,15),las=1)
    
    adderrorbars(x=toplot$Date,y=toplot$totmass.mean,SE=toplot$totmass.standard.error,
                 direction="updown",col=COL[toplot$W_treatment])
    
    points(totmass.mean~Date,data=toplot,add=T,pch=21,cex=1.5,legend=F,col="black",bg=COL[toplot$W_treatment])       
    
    # rect(xleft=as.Date("2016-01-28"),ybottom=-10,xright=as.Date("2016-02-09"),ytop=20,col=alpha("lightgrey",0.5))     
    rug(as.Date("2016-01-14"),lwd=3,line=-0.1)
    rug(as.Date("2016-02-24"),lwd=3,line=-0.1)
    # 
    # legend("topright",letters[i],bty="n",xpd=NA,cex=1.3)
    legend("topleft",paste("(",letters[i],")",sep=""),bty="n",cex=1.5,text.font=2)
    
    # axis.Date(side=1,at=seq.Date(from=min(dat.fast.day$Date),to=max(dat.fast.day$Date),by="day"),labels=F)
  }
  
  title(ylab=expression(Total~Plant~Mass~(g)),xlab="Date",
        outer=T,cex.lab=2)
  axis.Date(side=1,at=seq.Date(from=min(growth_dat$Date),to=max(growth_dat$Date)+5,by="week"),labels=T)
  
  legend("topright",c(expression(W[incr]),expression(W[const])),pt.bg=COL,cex=1.1,title="Treatment",bty="n",pch=21,pt.cex=1.1)
  
  #----------------------------------------------------------------------------------------------------
  dev.off()
  
  
  #---------------------------------------------------------------------------------------------------------------------------------------
  #---------------------------------------------------------------------------------------------------------------------------------------
  
  # plot height and diameter
  
  pdf(file="output/FigureS5.pdf",width=5,height=8)
  par(mfrow=c(6,1),mar=c(0.5,0,0,0),oma=c(4,7,1,1),cex.axix=1.5)
  
  
  # windows(40,70);par(mfrow=c(6,1),mar=c(0,0,0,0),oma=c(6,7,1,4))
  for (i in 1:length(growth_mean)){
    
    toplot <- growth_mean[[i]]
    
    
    
    plotBy(Height.mean~Date|W_treatment,data=toplot,type="l",lwd=2,col=COL,
           legend=F,ylim=c(0,100),las=1)
    
    adderrorbars(x=toplot$Date,y=toplot$Height.mean,SE=toplot$Height.standard.error,
                 direction="updown",col=COL[toplot$W_treatment])
    
    points(Height.mean~Date,data=toplot,add=T,pch=21,cex=1.5,legend=F,col="black",bg=COL[toplot$W_treatment])       
    
    # rect(xleft=as.Date("2016-01-28"),ybottom=-10,xright=as.Date("2016-02-09"),ytop=20,col=alpha("lightgrey",0.5))     
     rug(as.Date("2016-01-14"),lwd=3,line=-0.2)
     rug(as.Date("2016-02-24"),lwd=3,line=-0.2)
    # # 
    # legend("topright",letters[i],bty="n",xpd=NA,cex=1.3)
    legend("topleft",paste("(",letters[i],")",sep=""),bty="n",cex=1.5,text.font=2)
    
    # axis.Date(side=1,at=seq.Date(from=min(dat.fast.day$Date),to=max(dat.fast.day$Date),by="day"),labels=F)
  }
  
  title(ylab=expression(Plant~Height~(cm)),xlab="Date",
        outer=T,cex.lab=2)
  axis.Date(side=1,at=seq.Date(from=min(growth_dat$Date),to=max(growth_dat$Date)+5,by="week"),labels=T)
  
  legend("top",c(expression(W[incr]),expression(W[const])),ncol=2,pt.bg=COL,cex=1.1,title="Treatment",bty="n",pch=21,pt.cex=1.1)
  
  #----------------------------------------------------------------------------------------------------
  dev.off()
  
  
  #---------------------------------------------------------------------------------------------------------------------------------------
  #---------------------------------------------------------------------------------------------------------------------------------------
  
  # plot  diameter
  
  pdf(file="output/FigureS6.pdf",width=5,height=8)
  par(mfrow=c(6,1),mar=c(0.5,0,0,0),oma=c(4,7,1,1),cex.axix=1.5)
  
  
  # windows(40,70);par(mfrow=c(6,1),mar=c(0,0,0,0),oma=c(6,7,1,4))
  for (i in 1:length(growth_mean)){
    
    toplot <- growth_mean[[i]]
    
    
    
    plotBy(diam.mean~Date|W_treatment,data=toplot,type="l",lwd=2,col=COL,
           legend=F,ylim=c(0,8),las=1)
    
    adderrorbars(x=toplot$Date,y=toplot$diam.mean,SE=toplot$diam.standard.error,
                 direction="updown",col=COL[toplot$W_treatment])
    
    points(diam.mean~Date,data=toplot,add=T,pch=21,cex=1.5,legend=F,col="black",bg=COL[toplot$W_treatment])       
    
    # rect(xleft=as.Date("2016-01-28"),ybottom=-10,xright=as.Date("2016-02-09"),ytop=20,col=alpha("lightgrey",0.5))     
     rug(as.Date("2016-01-14"),lwd=3,line=-0.2)
     rug(as.Date("2016-02-24"),lwd=3,line=-0.2)
    # # 
    # legend("topright",letters[i],bty="n",xpd=NA,cex=1.3)
    legend("topleft",paste("(",letters[i],")",sep=""),bty="n",cex=1.5,text.font=2)
    
    # axis.Date(side=1,at=seq.Date(from=min(dat.fast.day$Date),to=max(dat.fast.day$Date),by="day"),labels=F)
  }
  
  title(ylab=expression(Stem~diameter~(mm)),xlab="Date",
        outer=T,cex.lab=2)
  axis.Date(side=1,at=seq.Date(from=min(growth_dat$Date),to=max(growth_dat$Date)+5,by="week"),labels=T)
  
  legend("top",c(expression(W[incr]),expression(W[const])),ncol=2,pt.bg=COL,cex=1.1,title="Treatment",bty="n",pch=21,pt.cex=1.1)
  
  #----------------------------------------------------------------------------------------------------
  dev.off()
  