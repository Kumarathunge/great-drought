# plot An-T response second campaign
#-------------------------------------------------------

#- get data for campaign 1 

#avt.1<-subset(avt.l,avt.l$campaign==1)

avt.l<-getAQ()

avt.l.list <- subset(avt.l, avt.l$campaign==2 & avt.l$LightFac %in% c(4) & avt.l$Prov=="B") 


with(avt.l.list,plot(Tleaf,Photo,col=W_treatment,pch=16,cex=2))
# Asatdata.l<-split(avt.l.list,paste(avt.l.list$W_treatment))

# AvTfits.list <- lapply(Asatdata.l,FUN=fitAvT)

pdf(file="output/Figure13-Photo_vs_Temperature_camp2.pdf",width=3.5,height=3)
par(mar=c(3.5,4,0.5,0.5),oma=c(0,0,0,0))

smoothplot(Tleaf, Photo, W_treatment,kgam=2,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(18,42),ylim=c(0,30),xlab="",ylab="",
           data=avt.l.list, axes=F)
#- add TREATMENT MEANS for mass
dat3 <- summaryBy(Photo+Cond+Tair+Tleaf~Room+W_treatment,FUN=c(mean,standard.error),data=avt.l.list,na.rm=T)

adderrorbars(x=dat3$Tleaf.mean,y=dat3$Photo.mean,SE=dat3$Photo.standard.error,direction="updown")
adderrorbars(x=dat3$Tleaf.mean,y=dat3$Photo.mean,SE=dat3$Tleaf.standard.error,direction="leftright")

#plotBy(Photo.mean~Tleaf.mean|location,data=dat3,add=T,pch=21,cex=2,legend=F,col="black")
palette(COL) 
points(Photo.mean~Tleaf.mean,data=dat3,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL)

#- gussy up the graph
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2)
title(xlab=expression(Measurement~T[leaf]~(degree*C)),cex.lab=1.3,line=2)
title(ylab=expression(A[sat]~(mu*mol~m^-2~s^-1)),cex.lab=1.3,line=2)
legend("bottomleft",c("Wet","Dry"),fill=COL,cex=0.9,title="Treatment",bty="n")

dev.off()
