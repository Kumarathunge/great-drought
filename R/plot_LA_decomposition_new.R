# with(massdata,plot(Tair,leafarea,col=W_treatment,pch=16))
# with(massdata,plot(Tair,leafno,col=W_treatment,pch=16))
# 
# with(massdata,plot(Tair,leafarea/leafdm,col=W_treatment,pch=16))
# 
# with(massdata,plot(Tair,leafdm,col=W_treatment,pch=16))
# 

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#- This plots temperature response curves for biomass partitioning data (from final mass) 
#- Figure x
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

#- prepare the three datasets, fit temperature response curves in preparation for plotting


#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#- prepare the three datasets, fit temperature response curves in preparation for plotting

#--- Biomass
#- prepare biomass data, fit and plot the first panel
dat <- getHarvest()
size <- getSize()

#- pull out just the unique pot and room numbers from teh size dataframe
size2 <- unique(size[,c("Code","Room","W_treatment","location","Tair")])

#- merge pot ids and harvest. Note the pre-treatment plants get excluded here
dat2 <- merge(size2,dat,by.x=c("Code","location","W_treatment"),by.y=c("Code","location","W_treatment"))
massdata <- subset(dat2,location == "Warm-edge" & Date==as.Date("2016-02-22"))

# SLA
massdata$sla<-with(massdata,leafarea/leafdm)

# mean leaf size
massdata$leafsize<-with(massdata,leafarea/leafno)

# mean leaf mass fraction
massdata$lmr<-with(massdata,leafdm/totdm)

# fit T-response of total leaf area

#- fit all the curves
# fit temperature response

massdata.l <- split(massdata,massdata$W_treatment)

MASSvTfits.leaf <- lapply(massdata.l,FUN=fitJuneT,start=list(Rref=800,Topt=30,theta=5),namey="leafarea",namex="Tair",lengthPredict=20)

LA.pred <- data.frame(do.call(rbind,
                              list(MASSvTfits.leaf[[1]][[2]],MASSvTfits.leaf[[2]][[2]])))
LA.pred$W_treatment <- c(rep("Wet",nrow(LA.pred)/2),rep("Dry",nrow(LA.pred)/2))
LA.pred$W_treatment <- factor(LA.pred$W_treatment,levels=c("Wet","Dry"))  



#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

# Extract Topt

LA.topt <- data.frame(do.call(rbind,
                              list(MASSvTfits.leaf[[1]][[1]],MASSvTfits.leaf[[2]][[1]])))
LA.ci <- data.frame(do.call(rbind,
                              list(MASSvTfits.leaf[[1]][[3]],MASSvTfits.leaf[[2]][[3]])))

#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------


#- get TREATMENT MEANS for mass
dat3 <- summaryBy(leafarea+sla+leafsize+leafno+leafdm+lmr+Tair~Room+W_treatment,FUN=c(mean,standard.error),data=massdata,na.rm=T)




pdf(file="output/Figure4.1-leafarea_size_no.pdf",width=6,height=6)

par(mar=c(1,4,1,1),mfrow=c(2,2),cex.lab=1.5,cex.axis=1.2,oma=c(4,0,0,0))

palette(rev(brewer.pal(6,"Spectral")))
COL=palette()[c(1,6)]
# palette(COL) 

xlims <- c(15,40)


#- plot standing leaf area 

plotBy(Sim.Mean~Tleaf|W_treatment,data=LA.pred,legend=F,type="l",las=1,ylim=c(0,1500),lwd=3,cex.lab=1.4,xlim=xlims,axes=F,
       ylab="",col=COL,
       xlab="")
as.m <- subset(LA.pred,W_treatment=="Wet")
bs.m <- subset(LA.pred,W_treatment=="Dry")

polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)

#- add TREATMENT MEANS for mass

plotBy(leafarea.mean~Tair.mean|W_treatment,data=dat3,add=T,pch=16,cex=0.5,legend=F,col=COL,
       panel.first=(adderrorbars(x=dat3$Tair.mean,y=dat3$leafarea.mean,
                                 SE=dat3$leafarea.standard.error,direction="updown")))

points(leafarea.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[c(1,2)])

#- gussy up the graph

magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn=6)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,line=2.5)
title(ylab=expression(Total~Leaf~Area~(cm^2)),line=2.5,cex.lab=1.3)
legend("topright",paste("(",letters[1],")",sep=""),bty="n",cex=1.2,text.font=1.1)
legend("topleft",c(expression(W[incr]),expression(W[const])),pt.bg=COL,cex=1.1,title="Treatment",bty="n",pch=21,pt.cex=1.1)

#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------

# T-Response of leaf size

#- fit all the curves
LSfits.l <- lapply(massdata.l,FUN=fitJuneT,namex="Tair",namey="leafsize",lengthPredict=21,start=list(Rref=30,Topt=25,theta=10))

LS.pred <- data.frame(do.call(rbind,
                              list(LSfits.l[[1]][[2]],LSfits.l[[2]][[2]])))
LS.pred$W_treatment <- c(rep("Wet",nrow(LS.pred)/2),rep("Dry",nrow(LS.pred)/2))
LS.pred$W_treatment <- factor(LS.pred$W_treatment,levels=c("Wet","Dry"))  
#----------------------------------------------------------------------------------------------------

LS.topt <- data.frame(do.call(rbind,
                              list(LSfits.l[[1]][[1]],LSfits.l[[2]][[1]])))
LS.ci <- data.frame(do.call(rbind,
                              list(LSfits.l[[1]][[3]],LSfits.l[[2]][[3]])))

#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------



plotBy(Sim.Mean~Tleaf|W_treatment,data=LS.pred,legend=F,type="l",las=1,ylim=c(0,40),lwd=3,cex.lab=2,xlim=c(15,40),axes=F,
       ylab="",col=COL,
       xlab="")
as.m <- subset(LS.pred,W_treatment=="Wet")
bs.m <- subset(LS.pred,W_treatment=="Dry")

polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)
#legend("topleft",levels(SLA.pred$location),fill=COL,cex=1.7,title="Provenance")

#- Leaf size
plotBy(leafsize.mean~Tair.mean|W_treatment,data=dat3,las=1,legend=F,pch=16,cex=1,
       axes=F,xlab="",ylab="",col=COL,add=T,
       panel.first=adderrorbars(x=dat3$Tair.mean,y=dat3$leafsize.mean,SE=dat3$leafsize.standard.error,direction="updown"))

palette(COL) 
points(leafsize.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=COL[W_treatment])


magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)

title(ylab=expression(Mean~Leaf~Size~(cm^2)),outer=F,cex.lab=1.3,line=2)
legend("topright",paste("(",letters[2],")",sep=""),bty="n",cex=1.2,text.font=1.1)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=F,line=2.5)


#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------

# T-Response of leaf no

#- fit all the curves
Lnfits.l <- lapply(massdata.l,FUN=fitJuneT,namex="Tair",namey="leafno",lengthPredict=21,start=list(Rref=30,Topt=25,theta=10))

Ln.pred <- data.frame(do.call(rbind,
                              list(Lnfits.l[[1]][[2]],Lnfits.l[[2]][[2]])))
Ln.pred$W_treatment <- c(rep("Wet",nrow(Ln.pred)/2),rep("Dry",nrow(Ln.pred)/2))
Ln.pred$W_treatment <- factor(Ln.pred$W_treatment,levels=c("Wet","Dry"))  
#----------------------------------------------------------------------------------------------------

plotBy(Sim.Mean~Tleaf|W_treatment,data=Ln.pred,legend=F,type="l",las=1,ylim=c(0,80),lwd=3,cex.lab=2,xlim=c(15,40),axes=F,
       ylab="",col=COL,
       xlab="")
as.m <- subset(Ln.pred,W_treatment=="Wet")
bs.m <- subset(Ln.pred,W_treatment=="Dry")

polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)
#legend("topleft",levels(SLA.pred$location),fill=COL,cex=1.7,title="Provenance")

#- Leaf size
plotBy(leafno.mean~Tair.mean|W_treatment,data=dat3,las=1,legend=F,pch=16,cex=1,
       axes=F,xlab="",ylab="",col=COL,add=T,
       panel.first=adderrorbars(x=dat3$Tair.mean,y=dat3$leafno.mean,SE=dat3$leafno.standard.error,direction="updown"))

palette(COL) 
points(leafno.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=COL[W_treatment])


magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)

title(ylab=expression(Total~Leaf~Count),outer=F,cex.lab=1.3,line=2)
legend("topright",paste("(",letters[3],")",sep=""),bty="n",cex=1.2,text.font=1.1)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=F,line=2.5)
title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,line=1.3,xpd=NA,outer=T,adj=.225)

#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------

# T-Response of SLA

#- fit NAR response curves
slafits.l <- list()
for(i in 1:length(massdata.l)){
  slafits.l[[i]] <- lm(sla ~Tair,data=massdata.l[[i]])
}

plotBy(sla.mean~Tair.mean|W_treatment,data=dat3,las=1,xlim=xlims,ylim=c(100,300),type="n",legend=F,axes=F,ylab="",xlab="")
predline(slafits.l[[1]],col=alpha(COL[1],0.5))
predline(slafits.l[[2]],col=alpha(COL[2],0.5))

plotBy(sla.mean~Tair.mean|W_treatment,data=dat3,las=1,xlim=xlims,ylim=c(100,300),legend=F,pch=16,cex=.2,
       axes=F,xlab="",ylab="",col=COL,add=T,
       panel.first=adderrorbars(x=dat3$Tair.mean,y=dat3$sla.mean,SE=dat3$sla.standard.error,direction="updown"))
points(sla.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=COL[W_treatment])
magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)

title(ylab=expression(SLA~(cm^2~g^-1)),outer=F,cex.lab=1.3,line=2.3)
legend("topright",paste("(",letters[4],")",sep=""),bty="n",cex=1.2,text.font=1.1)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=F,line=2.5)
title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,line=1.3,outer=T,xpd=NA,adj=.8725)

dev.off()

# #----------------------------------------------------------------------------------------------------------------------------
# #----------------------------------------------------------------------------------------------------------------------------
# 
# # T-Response of leaf dry mass
# 
# #- fit all the curves
# Ldmfits.l <- lapply(massdata.l,FUN=fitJuneT,namex="Tair",namey="leafdm",lengthPredict=21,start=list(Rref=30,Topt=25,theta=10))
# 
# Ldm.pred <- data.frame(do.call(rbind,
#                               list(Ldmfits.l[[1]][[2]],Ldmfits.l[[2]][[2]])))
# Ldm.pred$W_treatment <- c(rep("Wet",nrow(Ldm.pred)/2),rep("Dry",nrow(Ldm.pred)/2))
# Ldm.pred$W_treatment <- factor(Ldm.pred$W_treatment,levels=c("Wet","Dry"))  
# #----------------------------------------------------------------------------------------------------
# 
# plotBy(Sim.Mean~Tleaf|W_treatment,data=Ldm.pred,legend=F,type="l",las=1,ylim=c(0,10),lwd=3,cex.lab=2,xlim=c(15,40),axes=F,
#        ylab="",col=COL,
#        xlab="")
# as.m <- subset(Ldm.pred,W_treatment=="Wet")
# bs.m <- subset(Ldm.pred,W_treatment=="Dry")
# 
# polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
# polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)
# #legend("topleft",levels(SLA.pred$location),fill=COL,cex=1.7,title="Provenance")
# 
# #- Leaf size
# plotBy(leafdm.mean~Tair.mean|W_treatment,data=dat3,las=1,legend=F,pch=16,cex=1,
#        axes=F,xlab="",ylab="",col=COL,add=T,
#        panel.first=adderrorbars(x=dat3$Tair.mean,y=dat3$leafdm.mean,SE=dat3$leafdm.standard.error,direction="updown"))
# 
# palette(COL) 
# points(leafdm.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=COL[W_treatment])
# 
# 
# magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)
# 
# title(ylab=expression(Leaf~mass~(g)),outer=F,cex.lab=1.5,line=2)
# legend("topright",paste("(",letters[4],")",sep=""),bty="n",cex=1.5,text.font=1.1)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=F,line=2.5)
# 

# # T-Response of leaf mass ratio
# 
# #- fit all the curves
# Lmrfits.l <- lapply(massdata.l,FUN=fitJuneT,namex="Tair",namey="lmr",lengthPredict=21,start=list(Rref=0.5,Topt=25,theta=10))
# 
# Lmr.pred <- data.frame(do.call(rbind,
#                                list(Lmrfits.l[[1]][[2]],Lmrfits.l[[2]][[2]])))
# Lmr.pred$W_treatment <- c(rep("Wet",nrow(Lmr.pred)/2),rep("Dry",nrow(Ldm.pred)/2))
# Lmr.pred$W_treatment <- factor(Lmr.pred$W_treatment,levels=c("Wet","Dry"))  
# #----------------------------------------------------------------------------------------------------
# 
# #----------------------------------------------------------------------------------------------------
# 
# plotBy(Sim.Mean~Tleaf|W_treatment,data=Lmr.pred,legend=F,type="l",las=1,ylim=c(0,1),lwd=3,cex.lab=2,xlim=c(15,40),axes=F,
#        ylab="",col=COL,
#        xlab="")
# as.m <- subset(Lmr.pred,W_treatment=="Wet")
# bs.m <- subset(Lmr.pred,W_treatment=="Dry")
# 
# polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
# polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)
# #legend("topleft",levels(SLA.pred$location),fill=COL,cex=1.7,title="Provenance")
# 
# #- Leaf size
# plotBy(lmr.mean~Tair.mean|W_treatment,data=dat3,las=1,legend=F,pch=16,cex=1,
#        axes=F,xlab="",ylab="",col=COL,add=T,
#        panel.first=adderrorbars(x=dat3$Tair.mean,y=dat3$lmr.mean,SE=dat3$lmr.standard.error,direction="updown"))
# 
# palette(COL) 
# points(leafdm.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=COL[W_treatment])
# 
# 
# magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)
# 
# title(ylab=expression(Leaf~mass~(g)),outer=F,cex.lab=1.5,line=2)
# legend("topright",paste("(",letters[4],")",sep=""),bty="n",cex=1.5,text.font=1.1)
# title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=F,line=2.5)
# 
# #----------------------------------------------------------------------------------------------------------------------------
# #----------------------------------------------------------------------------------------------------------------------------







# smoothplot(Tair, sla, W_treatment,kgam=1,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
#            #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
#            polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
#            linecol=c("black","red"),pointcols=NA,
#            cex=1,main="",
#            xlim=c(15,40),ylim=c(50,300),xlab="",ylab="",
#            data=massdata, axes=F)
# 
# #- overlay points
# plotBy(sla.mean~Tair.mean|W_treatment,data=dat3,las=1,ylim=c(50,300),legend=F,pch=16,cex=0.5,col=COL,add=T,
#        panel.first=adderrorbars(x=dat3$Tair.mean,
#                                 y=dat3$sla.mean,
#                                 SE=dat3$sla.standard.error,direction="updown"),
#        axes=F,cex.lab=2)
# 
# palette(COL) 
# points(sla.mean~Tair.mean,data=dat3,add=T,pch=21,cex=1.4,legend=F,col="black",bg=W_treatment)
# 
# magaxis(side=1:4,labels=c(1,1,0,0),las=1,cex.axis=1.2,ratio=0.4,tcl=0.2,majorn=4)
# 
# 



