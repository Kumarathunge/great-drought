# read data

hydro<-read.csv("Data/Glasshouse_DRAKE_EUTE_THERMAL-NICHE/data/GHS39_GREAT_MAIN_WATERPOTENTIAL-HYDRO_20160224_L2.csv")
hydro.l<-summaryBy(Ppred+Pmidxyl+Pmidleaf+Et_gmin+Et_ghr+ki+kmax+xylem_area+trans_leaf+Tair+plc~Room+W_treatment, FUN=c(mean,std.error),
                   data=hydro)

# pdf(file="output/Figure11-Hydraulics.pdf",width=7.3,height=8)

pdf(file="output/Figure11-Hydraulics.pdf",width=6,height=3)
par(mar=c(3.5,4,0.5,0.5),oma=c(0,0,0,0),mfrow=c(1,2))
palette(rev(brewer.pal(6,"Spectral")))
COL=palette()[c(6,1)]

#- plot pre dawn leaf water potential
smoothplot(Tair, Ppred/10, W_treatment,kgam=2,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,1),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(Ppred.mean/10~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,1),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$Ppred.mean/10,
                                SE=hydro.l$Ppred.std.error/10,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(Ppred.mean/10~Tair.mean,data=hydro.l,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[W_treatment])


title(ylab=expression(Leaf~Psi[pd]~(-MPa)),xpd=NA,cex.lab=1.2,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[1],cex=1.2,bty="n")
# legend("bottomright",c("Dry","Wet"),fill=COL,cex=1.2,title="Treatment",bty="n")
title(xlab=expression(Measurement~T[leaf]~(degree*C)),cex.lab=1.2,line=2)

#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------
# with(hydro,plot(Tair,Pmidleaf,col=W_treatment,pch=16,cex=2))
#- plot midday water potential

smoothplot(Tair, Pmidleaf/10, W_treatment,kgam=4,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,3),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(Pmidleaf.mean/10~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,3),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$Pmidleaf.mean/10,
                                SE=hydro.l$Pmidleaf.std.error/10,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(Pmidleaf.mean/10~Tair.mean,data=hydro.l,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[W_treatment])


title(ylab=expression(Leaf~Psi[md]~(-MPa)),xpd=NA,cex.lab=1.2,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[2],cex=1.2,bty="n")
legend("bottomright",c("Dry","Wet"),fill=COL,cex=1.2,title="Treatment",bty="n")
title(xlab=expression(Measurement~T[leaf]~(degree*C)),cex.lab=1.2,line=2)
dev.off()


#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------
#- plot mid day xylum water potential
# with(hydro,plot(Tair,Pmidxyl/10,col=W_treatment,pch=16,cex=2))
pdf(file="output/FigureS3-Hydraulics.pdf",width=5,height=5)
par(mar=c(3.5,4,0.5,0.5),oma=c(0,0,0,0),mfrow=c(2,2))


smoothplot(Tair, Pmidxyl/10, W_treatment,kgam=2,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,2),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(Pmidxyl.mean/10~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,2),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$Pmidxyl.mean/10,
                                SE=hydro.l$Pmidxyl.std.error/10,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(Pmidxyl.mean/10~Tair.mean,data=hydro.l,add=T,pch=21,cex=2,legend=F,col="black",bg=COL[W_treatment])


title(ylab=expression(Stem~Psi[md]~(-MPa)),xpd=NA,cex.lab=1.3,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[1],cex=1.2,bty="n")

title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,adj=0.5,line=2)
#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------

# with(hydro,plot(Tair,trans_leaf,col=W_treatment,pch=16,cex=2))
#- plot total leaf transpiration

smoothplot(Tair, trans_leaf, W_treatment,kgam=6,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,600),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(trans_leaf.mean~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,3),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$trans_leaf.mean,
                                SE=hydro.l$trans_leaf.std.error,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(trans_leaf.mean~Tair.mean,data=hydro.l,add=T,pch=21,cex=2,legend=F,col="black",bg=COL[W_treatment])


title(ylab=expression(E[leaf]~(g~m^-2~h^-1)),xpd=NA,cex.lab=1.3,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[2],cex=1.2,bty="n")
# legend("bottomright",c("Dry","Wet"),fill=COL,cex=1.2,title="Treatment",bty="n")
title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,adj=0.8,line=2)

#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------
#- plot total plant transpiration
 # with(hydro,plot(Tair,Et_ghr,col=W_treatment,pch=16,cex=2))


smoothplot(Tair, Et_ghr, W_treatment,kgam=6,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,50),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(Et_ghr.mean~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,2),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$Et_ghr.mean,
                                SE=hydro.l$Et_ghr.std.error,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(Et_ghr.mean~Tair.mean,data=hydro.l,add=T,pch=21,cex=2,legend=F,col="black",bg=COL[W_treatment])


title(ylab=expression(E[total]~(g~hr^-1~plant^-1)),xpd=NA,cex.lab=1.3,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[3],cex=1.2,bty="n")

title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,adj=0.5,line=1.8)

#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------
#- plot PLC

# with(hydro,plot(Tair,plc,col=W_treatment,pch=16,cex=2))
# legend("topleft",c("Dry","Wet"),fill=COL,cex=1.2,title="Treatment",bty="n")
# 

smoothplot(Tair, plc, W_treatment,kgam=4,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(15,40),ylim=c(0,40),xlab="",ylab="",
           data=hydro, axes=F)


#- overlay points
plotBy(plc.mean~Tair.mean|W_treatment,data=hydro.l,las=1,ylim=c(0,40),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=hydro.l$Tair.mean,
                                y=hydro.l$plc.mean,
                                SE=hydro.l$plc.std.error,direction="updown"),
       axes=F,xlab="Tair",cex.lab=2)



palette(COL) 
points(plc.mean~Tair.mean,data=hydro.l,add=T,pch=21,cex=2,legend=F,col="black",bg=COL[W_treatment])


title(ylab="PLC (%)",xpd=NA,cex.lab=1.3,line=2)

magaxis(side=1:4,labels=c(1,1,0,0),las=1)
legend("topright",letters[4],cex=1.2,bty="n")

title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.3,adj=0.8,line=1.8)

dev.off()















# legend("bottomright",c("Dry","Wet"),fill=COL,cex=1.2,title="Treatment",bty="n")


# #- plot pre-dawn leaf water potential data
# 
# hydro.l$Ppred_L<-with(hydro.l,Ppred.mean-1.96*Ppred.std.error)
# hydro.l$Ppred_U<-with(hydro.l,Ppred.mean+1.96*Ppred.std.error)
# 
# 
# ppred<-tidyr::spread(hydro.l[c(1:3)], Room, Ppred.mean )
# ppred[,1]<-NULL
# ppred<-as.matrix(ppred)
# 
# ppredci_l<-spread(hydro.l[c(1,2,21)], Room, Ppred_L )
# ppredci_l[,1]<-NULL
# ppredci_l<-as.matrix(ppredci_l)
# 
# ppredci_u<-spread(hydro.l[c(1,2,22)], Room, Ppred_U )
# ppredci_u[,1]<-NULL
# ppredci_u<-as.matrix(ppredci_u)
# 
# palette(rev(brewer.pal(6,"Spectral")))
# COL=palette()[c(6,1)]
# 
# barplot2(height=ppred,beside=T,plot.ci = T, ci.l = ppredci_l,ci.u=ppredci_u,yaxt="n",
#          ci.width=0.2,col=COL,
#          names.arg=c("18","21.5","25","28.5","32","35.5"),ylim=c(0,15))
# 
# magaxis(side=c(2,4),labels=c(1,0),frame.plot=T)
# title(xlab=expression(Growth~T[air]~(degree*C)),outer=F,line=2.3,cex.lab=1.3)
# # title(ylab=expression(VWC~(m^3~m^-3)),line=2.2,cex.lab=1.3)
# title(ylab="VWC (%)",line=2.2,cex.lab=1.3)
# 
# legend("topleft",legend=c("Dry","Wet"),bty="n",ncol=1,fill=COL,title="Treatment",cex=1.3)
# 
