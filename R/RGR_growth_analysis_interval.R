#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#-- Growth analysis script focusing on the 11-day growth INVERVAL. 
#       returnRGR() does most of the heavy data-manipulation work
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------




#-----------------------------------------------------------------------------------------
#- get the data, process it for RGR.
dat.list <- returnRGR(plotson=F)
dat <- dat.list[[2]]     # RGR and AGR merged with canopy leaf area and SLA for the intensive growth interval only
dat.all <- dat.list[[1]] #RGR and AGR caculated for all available data.

dat$LAR <- dat$LAR/1000 # convert back to m2 g-1
#-----------------------------------------------------------------------------------------

# get drought data processed for AGR
agr_d<-returnAGR_Drought()
agr_d$LAR <- agr_d$LAR/1000 # convert back to m2 g-1
# add well watered and dry data together
dat<-rbind(dat,agr_d)



#-----------------------------------------------------------------------------------------
#- fit RGR, LAR, and NAR v T to estimate Topts
tofit <- subset(dat,location=="Warm-edge") # not needed, as this is done in returnRGR() now.
tofit.l <- split(tofit,tofit$W_treatment)

#- fit AGR and RGR T response curves
RGRvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=0.12,Topt=25,theta=12),namey="RGR",namex="Tair",lengthPredict=20)
LARvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=0.020,Topt=30,theta=5),namey="LAR",namex="Tair",lengthPredict=20)


#- fit NAR response curves
NARfits.l <- list()
for(i in 1:length(tofit.l)){
  NARfits.l[[i]] <- lm(NAR ~Tair,data=tofit.l[[i]])
  
}
#-----------------------------------------------------------------------------------------





#-----------------------------------------------------------------------------------------
#- average across Water treatments
dat2 <- summaryBy(RGR+AGR+SLA+LAR+NAR+LMF+Tair~Room+W_treatment,FUN=c(mean,standard.error),data=dat,na.rm=T)
#-----------------------------------------------------------------------------------------






#-----------------------------------------------------------------------------------------
#- Make a 3-panel plot showing RGR, LAR, and NAR relative to growth temperature.
pdf(file="output/Figure5_RGR_LAR_NAR_interval.pdf",width=3.5,height=8)

#windows(30,60)
par(mfrow=c(3,1),mar=c(3,4,1,1),oma=c(3,4,.5,.5))
palette(rev(brewer.pal(6,"Spectral")))
COL=palette()[c(1,6)]

ptsize <- 1.5
xlims <- c(17,37)


#------------
#- plot the First panel (RGR vs. Temperature)
RGRplot <- data.frame(do.call(rbind,
                              list(RGRvTfits.l[[1]][[2]],RGRvTfits.l[[2]][[2]])))
RGRplot$W_treatment <- c(rep("Wet",nrow(RGRplot)/2),rep("Dry",nrow(RGRplot)/2))
RGRplot$W_treatment <- factor(RGRplot$W_treatment,levels=c("Wet","Dry"))  

plotBy(Sim.Mean~Tleaf|W_treatment,data=RGRplot,legend=F,type="l",las=1,xlim=xlims,ylim=c(0,0.15),lwd=3,cex.lab=2,col=COL,
       ylab="",axes=F,
       xlab="")
as.rgr <- subset(RGRplot,W_treatment=="Wet")
bs.rgr <- subset(RGRplot,W_treatment=="Dry")


polygon(x = c(as.rgr$Tleaf, rev(as.rgr$Tleaf)), y = c(as.rgr$Sim.97.5., rev(as.rgr$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(bs.rgr$Tleaf, rev(bs.rgr$Tleaf)), y = c(bs.rgr$Sim.97.5., rev(bs.rgr$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)


#- add TREATMENT MEANS
plotBy(RGR.mean~Tair.mean|W_treatment,data=dat2,las=1,xlim=c(17,37),ylim=c(0,0.5),legend=F,legendwhere="bottomleft",pch=16,
       axes=F,xlab="",ylab="",cex=ptsize,col=COL,add=T,
       panel.first=adderrorbars(x=dat2$Tair.mean,y=dat2$RGR.mean,SE=dat2$RGR.standard.error,direction="updown"))
palette(COL) 
points(RGR.mean~Tair.mean,data=dat2,add=T,pch=21,cex=2,legend=F,col="black",bg=W_treatment)


magaxis(side=1:4,labels=c(0,1,0,0),las=1,cex.axis=2,minorn=2,ratio=0.25)
axis(side=1,at=c(20,25,30,35),labels=T,tick=F,cex.axis=2)
legend("topright",paste("(",letters[1],")",sep=""),bty="n",cex=1.5,text.font=2)
legend("bottomleft",c("Wet","Dry"),fill=COL,cex=1.2,title="Treatment",bty="n")

#legend("bottomleft",levels(dat2$location),fill=COL,cex=1.2,title="",bty="n")
#effect of warming
dat_w<-subset(dat2,dat2$Room %in% c(4,5))

# segments(dat_w$Tair.mean[1], dat_w$RGR.mean[1], x1 = dat_w$Tair.mean[3], y1 =dat_w$RGR.mean[3],lty=2,"black",lwd=2)
# segments(dat_w$Tair.mean[1], dat_w$RGR.mean[1], x1 = dat_w$Tair.mean[4], y1 =dat_w$RGR.mean[4],lty=2,"black",lwd=2)
# 

#------------
#- plot the Second panel (LAR vs. Temperature)

LARplot <- data.frame(do.call(rbind,
                              list(LARvTfits.l[[1]][[2]],LARvTfits.l[[2]][[2]])))
#LARplot$prov <- c(rep("A",nrow(LARplot)/3),rep("B",nrow(LARplot)/3),rep("C",nrow(LARplot)/3))
LARplot$W_treatment <- c(rep("Wet",nrow(LARplot)/2),rep("Dry",nrow(LARplot)/2))
LARplot$W_treatment <- factor(LARplot$W_treatment,levels=c("Wet","Dry"))  

plotBy(Sim.Mean~Tleaf|W_treatment,data=LARplot,legend=F,type="l",las=1,xlim=xlims,ylim=c(0,0.03),lwd=3,cex.lab=2,col=COL,
       ylab="",axes=F,
       xlab="")
w.rgr <- subset(LARplot,W_treatment=="Wet")
d.rgr <- subset(LARplot,W_treatment=="Dry")

polygon(x = c(w.rgr$Tleaf, rev(w.rgr$Tleaf)), y = c(w.rgr$Sim.97.5., rev(w.rgr$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(d.rgr$Tleaf, rev(d.rgr$Tleaf)), y = c(d.rgr$Sim.97.5., rev(d.rgr$Sim.2.5.)), col = alpha(COL[2],0.5), border = NA)

#- add TREATMENT MEANS
plotBy(LAR.mean~Tair.mean|W_treatment,data=dat2,las=1,xlim=c(17,37),legend=F,pch=16,
       axes=F,xlab="",ylab="",cex=ptsize,col=COL,add=T,
       panel.first=adderrorbars(x=dat2$Tair.mean,y=dat2$LAR.mean,SE=dat2$LAR.standard.error,direction="updown"))
points(LAR.mean~Tair.mean,data=dat2,add=T,pch=21,cex=2,legend=F,col="black",bg=W_treatment)
magaxis(side=1:4,labels=c(0,1,0,0),las=1,cex.axis=2,ratio=0.25,majorn=3)
axis(side=1,at=c(20,25,30,35),labels=T,tick=F,cex.axis=2)
legend("topright",paste("(",letters[2],")",sep=""),bty="n",cex=1.5,text.font=2)




#-----------------------------------------------------------------------------------------
#- NAR
plotBy(NAR.mean~Tair.mean|W_treatment,data=dat2,las=1,xlim=xlims,ylim=c(0,8),type="n",legend=F,axes=F,ylab="")
predline(NARfits.l[[1]],col=alpha(COL[1],0.5))
predline(NARfits.l[[2]],col=alpha(COL[2],0.5))

plotBy(NAR.mean~Tair.mean|W_treatment,data=dat2,las=1,xlim=c(17,37),ylim=c(0,10),legend=F,pch=16,cex=2,
       axes=F,xlab="",ylab="",col=COL,add=T,
       panel.first=adderrorbars(x=dat2$Tair.mean,y=dat2$NAR.mean,SE=dat2$NAR.standard.error,direction="updown"))
points(NAR.mean~Tair.mean,data=dat2,add=T,pch=21,cex=2,legend=F,col="black",bg=W_treatment)

magaxis(side=1:4,labels=c(0,1,0,0),las=1,cex.axis=2,ratio=0.25)
axis(side=1,at=c(20,25,30,35),labels=T,tick=F,cex.axis=2)

legend("topright",paste("(",letters[3],")",sep=""),bty="n",cex=1.5,text.font=2)
#-----------------------------------------------------------------------------------------



#- add axis labels
title(ylab=expression(RGR~(g~g^-1~d^-1)),outer=T,adj=0.95,cex.lab=2,line=0.5)
title(ylab=expression(LAR~(m^2~g^-1)),outer=T,adj=0.5,cex.lab=2,line=0.5)
title(ylab=expression(NAR~(g~m^-2~d^-1)),outer=T,adj=0.1,cex.lab=2,line=0.5)
title(xlab=expression(Growth~T[air]~(degree*C)),outer=T,adj=0.65,cex.lab=2,line=1.5)

#------------

dev.off()
#dev.copy2pdf(file="output/Figure5_RGR_LAR_NAR_interval.pdf")


