
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#-- This script makes a table of all of the temperature response parameters
#   AGR, RGR, Asat-growth,etc.
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------




#-------------------------------------------------------

#- get data for campaign 1 

avt.l<-getAQ()

avt.l.list <- subset(avt.l, avt.l$campaign==1 & avt.l$LightFac %in% c(4) & avt.l$Prov=="B") 
avt.l.list.l <- subset(avt.l, avt.l$campaign==1 & avt.l$LightFac %in% c(1,2) & avt.l$Prov=="B") 
avt.l.list.h <- subset(avt.l, avt.l$campaign==1 & avt.l$LightFac %in% c(3,4) & avt.l$Prov=="B") 

# with(avt.l.list,plot(Tleaf,Photo,col=W_treatment,pch=16,cex=2))
Asatdata.l<-split(avt.l.list,paste(avt.l.list$W_treatment))
Asatdata.lpar<-split(avt.l.list.l,paste(avt.l.list.l$W_treatment))
Asatdata.hpar<-split(avt.l.list.h,paste(avt.l.list.h$W_treatment))


#-------------------------------------------------------



#--- Biomass
#- prepare biomass data, fit and plot the first panel
dat <- getHarvest()
size <- getSize()

#- pull out just the unique pot and room numbers from teh size dataframe
size2 <- unique(size[,c("Code","Room","W_treatment","location","Tair")])

#- merge pot ids and harvest. Note the pre-treatment plants get excluded here
dat2 <- merge(size2,dat,by.x=c("Code","location","W_treatment"),by.y=c("Code","location","W_treatment"))

massdata <- subset(dat2,location == "Warm-edge" & Date==as.Date("2016-02-22"))
massdata.l <- split(massdata,massdata$W_treatment)



#-------------------------------------------------------


#-----------------------------------------------------------------------------------------
#- get the data, process it for RGR.
dat.list <- returnRGR(plotson=F)
dat <- dat.list[[2]]     # RGR and AGR merged with canopy leaf area and SLA for the intensive growth interval only
dat$LAR <- dat$LAR/1000 # convert back to m2 g-1


# get drought data processed for AGR
agr_d<-returnAGR_Drought()
agr_d$LAR <- agr_d$LAR/1000 # convert back to m2 g-1
# add well watered and dry data together
dat<-rbind(dat,agr_d)

#agr <- subset(agr,Date==as.Date("2016-02-08"))
dat.all <- dat.list[[1]] #RGR and AGR caculated for all available data.
#-----------------------------------------------------------------------------------------




#-----------------------------------------------------------------------------------------
#- fit AGR T to estimate Topts
tofit <- subset(dat,location=="Warm-edge") 
# tofit$AGR[which(tofit$AGR==Inf)] = NA
# tofit$AGR[which(tofit$AGR==-Inf)] = NA

tofit.l <- split(tofit,tofit$W_treatment)

#- fit AGR and RGR T response curves




AvTfits.list <- lapply(Asatdata.l,FUN=fitAvT)
# AvTfits.list.l <- lapply(Asatdata.lpar,FUN=fitAvT)
# AvTfits.list.h <- lapply(Asatdata.hpar,FUN=fitAvT)
MASSvTfits.l <- lapply(massdata.l,FUN=fitJuneT,start=list(Rref=5,Topt=30,theta=5),namey="totdm",namex="Tair",lengthPredict=20)
MASSvTfits.leaf <- lapply(massdata.l,FUN=fitJuneT,start=list(Rref=5,Topt=30,theta=5),namey="leafdm",namex="Tair",lengthPredict=20)
MASSvTfits.stem <- lapply(massdata.l,FUN=fitJuneT,start=list(Rref=5,Topt=30,theta=5),namey="stemdm",namex="Tair",lengthPredict=20)
MASSvTfits.root <- lapply(massdata.l,FUN=fitJuneT,start=list(Rref=5,Topt=30,theta=10),namey="rootdm",namex="Tair",lengthPredict=20)



AGRvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=0.15,Topt=30,theta=5),namey="AGR",namex="Tair",lengthPredict=20)
RGRvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=0.15,Topt=30,theta=5),namey="RGR",namex="Tair",lengthPredict=20)
LARvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=10,Topt=27,theta=12),namey="LAR",namex="Tair",lengthPredict=20)
SLAvTfits.l <- lapply(tofit.l,FUN=fitJuneT,start=list(Rref=450,Topt=25,theta=20),namey="SLA",namex="Tair",lengthPredict=20)

# 
# AvTfits.list.l[[1]][[3]]
# AvTfits.list.l[[2]][[1]]
#-----------------------------------------------------------------------------------------
#- pull out the parameter means and SE's for table


#- AvT long term at high PAR
AvTfits_longterm <- data.frame(do.call(rbind,
                                       list(AvTfits.list[[2]][[1]],AvTfits.list[[1]][[1]])))
AvTfits_longtermQCI <- data.frame(do.call(rbind,
                                          list(AvTfits.list[[2]][[3]],AvTfits.list[[1]][[3]])))
names(AvTfits_longtermQCI) <- c("Aref.CI","Topt.CI","theta.CI")
AvTfits_longterm <- cbind(AvTfits_longterm,AvTfits_longtermQCI)
AvTfits_longterm$W_treatment <- levels(dat$W_treatment)
#-----------------------------------------------------------------------------------------
# 
# #- AvT long term at low PAR
# AvTfits_lowpar <- data.frame(do.call(rbind,
#                                        list(AvTfits.list.l[[2]][[1]],AvTfits.list.l[[1]][[1]])))
# AvTfits_lowparQCI <- data.frame(do.call(rbind,
#                                           list(AvTfits.list.l[[2]][[3]],AvTfits.list.l[[1]][[3]])))
# names(AvTfits_lowparQCI) <- c("Aref.CI","Topt.CI","theta.CI")
# AvTfits_lowpar <- cbind(AvTfits_lowpar,AvTfits_lowparQCI)
# AvTfits_lowpar$W_treatment <- levels(dat$W_treatment)
# #-----------------------------------------------------------------------------------------
# 
# #- AvT long term at high PAR
# AvTfits_highpar <- data.frame(do.call(rbind,
#                                        list(AvTfits.list.h[[2]][[1]],AvTfits.list.h[[1]][[1]])))
# AvTfits_highparQCI <- data.frame(do.call(rbind,
#                                           list(AvTfits.list.h[[2]][[3]],AvTfits.list.h[[1]][[3]])))
# names(AvTfits_highparQCI) <- c("Aref.CI","Topt.CI","theta.CI")
# AvTfits_highpar <- cbind(AvTfits_highpar,AvTfits_highparQCI)
# AvTfits_highpar$W_treatment <- levels(dat$W_treatment)
# 
# #-----------------------------------------------------------------------------------------


#- final mass
MASSvTfits <- data.frame(do.call(rbind,
                                 list(MASSvTfits.l[[1]][[1]],MASSvTfits.l[[2]][[1]])))
MASSvTfits$W_treatment <- levels(dat$W_treatment)
MASSvTfitsCI <- data.frame(do.call(rbind,
                                   list(MASSvTfits.l[[1]][[3]],MASSvTfits.l[[2]][[3]])))
names(MASSvTfitsCI) <- c("totdmref.CI","Topt.CI","theta.CI")
MASSvTfits <- cbind(MASSvTfits,MASSvTfitsCI)


#- final leaf mass
MASSvTfits.1 <- data.frame(do.call(rbind,
                                 list(MASSvTfits.leaf[[1]][[1]],MASSvTfits.leaf[[2]][[1]])))
MASSvTfits.1$W_treatment <- levels(dat$W_treatment)
MASSvTfits.lCI <- data.frame(do.call(rbind,
                                   list(MASSvTfits.leaf[[1]][[3]],MASSvTfits.leaf[[2]][[3]])))
names(MASSvTfits.lCI) <- c("leafdmref.CI","Topt.CI","theta.CI")
MASSvTfits.1 <- cbind(MASSvTfits.1,MASSvTfits.lCI)


#- final stem mass
MASSvTfits.2 <- data.frame(do.call(rbind,
                                   list(MASSvTfits.stem[[1]][[1]],MASSvTfits.stem[[2]][[1]])))
MASSvTfits.2$W_treatment <- levels(dat$W_treatment)
MASSvTfits.2CI <- data.frame(do.call(rbind,
                                     list(MASSvTfits.stem[[1]][[3]],MASSvTfits.stem[[2]][[3]])))
names(MASSvTfits.2CI) <- c("stemdmref.CI","Topt.CI","theta.CI")
MASSvTfits.2 <- cbind(MASSvTfits.2,MASSvTfits.2CI)


#- final root mass
MASSvTfits.3 <- data.frame(do.call(rbind,
                                   list(MASSvTfits.root[[1]][[1]],MASSvTfits.root[[2]][[1]])))
MASSvTfits.3$W_treatment <- levels(dat$W_treatment)
MASSvTfits.3CI <- data.frame(do.call(rbind,
                                     list(MASSvTfits.root[[1]][[3]],MASSvTfits.root[[2]][[3]])))
names(MASSvTfits.3CI) <- c("rootdmref.CI","Topt.CI","theta.CI")
MASSvTfits.3 <- cbind(MASSvTfits.3,MASSvTfits.3CI)



#--------------------------------------------------------------------------------------------------------
#- AGR
AGRvTfits <- data.frame(do.call(rbind,
                                list(AGRvTfits.l[[1]][[1]],AGRvTfits.l[[2]][[1]])))
AGRvTfits$W_treatment <- levels(dat$W_treatment)
AGRvTfitsCI <- data.frame(do.call(rbind,
                                  list(AGRvTfits.l[[1]][[3]],AGRvTfits.l[[2]][[3]])))
names(AGRvTfitsCI) <- c("AGRref.CI","Topt.CI","theta.CI")
AGRvTfits <- cbind(AGRvTfits,AGRvTfitsCI)

#-RGR
RGRvTfits <- data.frame(do.call(rbind,
                                list(RGRvTfits.l[[1]][[1]],RGRvTfits.l[[2]][[1]])))
RGRvTfits$W_treatment <- levels(dat$W_treatment)
RGRvTfitsCI <- data.frame(do.call(rbind,
                                  list(RGRvTfits.l[[1]][[3]],RGRvTfits.l[[2]][[3]])))
names(RGRvTfitsCI) <- c("RGRref.CI","Topt.CI","theta.CI")
RGRvTfits <- cbind(RGRvTfits,RGRvTfitsCI)

#-LAR
LARvTfits <- data.frame(do.call(rbind,
                                list(LARvTfits.l[[1]][[1]],LARvTfits.l[[2]][[1]])))
LARvTfits$W_treatment <- levels(dat$W_treatment)
LARvTfitsCI <- data.frame(do.call(rbind,
                                  list(LARvTfits.l[[1]][[3]],LARvTfits.l[[2]][[3]])))
names(LARvTfitsCI) <- c("LARref.CI","Topt.CI","theta.CI")
LARvTfits <- cbind(LARvTfits,LARvTfitsCI)


#-SLA
SLAvTfits <- data.frame(do.call(rbind,
                                list(SLAvTfits.l[[1]][[1]],SLAvTfits.l[[2]][[1]])))
SLAvTfits$W_treatment <- levels(dat$W_treatment)
SLAvTfitsCI <- data.frame(do.call(rbind,
                                  list(SLAvTfits.l[[1]][[3]],SLAvTfits.l[[2]][[3]])))
names(SLAvTfitsCI) <- c("SLAref.CI","Topt.CI","theta.CI")
SLAvTfits <- cbind(SLAvTfits,SLAvTfitsCI)

#-----------------------------------------------------------------------------------------



# # make a table 

table2 <- data.frame(Variable=c(rep("Final total mass",2),rep("Final leaf mass",2),rep("Final stem mass",2),rep("Final root mass",2),
                                rep("AGR",2),rep("RGR",2),rep("Asat",2),rep("LAR",2),rep("SLA",2)),Treatment=rep(c("Wet","Dry"),9),
Topt = NA, Rref = NA, omega=NA)



# final mass
table2[1:2,3] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits$Topt,se=MASSvTfits$Topt.CI)                     
table2[1:2,4] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits$totdmref,se=MASSvTfits$totdmref.CI,nchar1=2,nchar2=2)
table2[1:2,5] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits$theta,se=MASSvTfits$theta.CI)   

# final leaf mass
table2[3:4,3] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.1$Topt,se=MASSvTfits.1$Topt.CI)                     
table2[3:4,4] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.1$leafdmref,se=MASSvTfits.1$leafdmref.CI,nchar1=2,nchar2=2)
table2[3:4,5] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.1$theta,se=MASSvTfits.1$theta.CI)   

# final stem mass
table2[5:6,3] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.2$Topt,se=MASSvTfits.2$Topt.CI)                     
table2[5:6,4] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.2$stemdmref,se=MASSvTfits.2$stemdmref.CI,nchar1=2,nchar2=2)
table2[5:6,5] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.2$theta,se=MASSvTfits.2$theta.CI)   

# final root mass
table2[7:8,3] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.3$Topt,se=MASSvTfits.3$Topt.CI)                     
table2[7:8,4] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.3$rootdmref,se=MASSvTfits.3$rootdmref.CI,nchar1=2,nchar2=2)
table2[7:8,5] <- mktable(location=MASSvTfits$W_treatment,yvar=MASSvTfits.3$theta,se=MASSvTfits.3$theta.CI)   

# AGR
table2[9:10,3] <- mktable(location=AGRvTfits$W_treatment,yvar=AGRvTfits$Topt,se=AGRvTfits$Topt.CI)                     
table2[9:10,4] <- mktable(location=AGRvTfits$W_treatment,yvar=AGRvTfits$AGRref,se=AGRvTfits$AGRref.CI,nchar1=2,nchar2=2)
table2[9:10,5] <- mktable(location=AGRvTfits$W_treatment,yvar=AGRvTfits$theta,se=AGRvTfits$theta.CI)                    


# RGR
table2[11:12,3] <- mktable(location=RGRvTfits$W_treatment,yvar=RGRvTfits$Topt,se=RGRvTfits$Topt.CI)                     
table2[11:12,4] <- mktable(location=RGRvTfits$W_treatment,yvar=RGRvTfits$RGRref,se=RGRvTfits$RGRref.CI,nchar1=2,nchar2=2)
table2[11:12,5] <- mktable(location=RGRvTfits$W_treatment,yvar=RGRvTfits$theta,se=RGRvTfits$theta.CI)                    


#Asat-in situ
table2[13:14,3] <- mktable(location=AvTfits_longterm$W_treatment,yvar=AvTfits_longterm$Topt,se=AvTfits_longterm$Topt.CI)                    
table2[13:14,4] <- mktable(location=AvTfits_longterm$W_treatment,yvar=AvTfits_longterm$Aref,se=AvTfits_longterm$Aref.CI)                    
table2[13:14,5] <- mktable(location=AvTfits_longterm$W_treatment,yvar=AvTfits_longterm$theta,se=AvTfits_longterm$theta.CI) 


#LAR
table2[15:16,3] <- mktable(location=LARvTfits$W_treatment,yvar=LARvTfits$Topt,se=LARvTfits$Topt.CI)                    
table2[15:16,4] <- mktable(location=LARvTfits$W_treatment,yvar=LARvTfits$LARref,se=LARvTfits$LARref.CI)                    
table2[15:16,5] <- mktable(location=LARvTfits$W_treatment,yvar=LARvTfits$theta,se=LARvTfits$theta.CI)



# #Asat-in situ - low par
# table2[3:4,3] <- mktable(location=AvTfits_lowpar$W_treatment,yvar=AvTfits_lowpar$Topt,se=AvTfits_lowpar$Topt.CI)                    
# table2[3:4,4] <- mktable(location=AvTfits_lowpar$W_treatment,yvar=AvTfits_lowpar$Aref,se=AvTfits_lowpar$Aref.CI)                    
# table2[3:4,5] <- mktable(location=AvTfits_lowpar$W_treatment,yvar=AvTfits_lowpar$theta,se=AvTfits_lowpar$theta.CI) 


#SLA
table2[17:18,3] <- mktable(location=SLAvTfits$W_treatment,yvar=SLAvTfits$Topt,se=SLAvTfits$Topt.CI)                    
table2[17:18,4] <- mktable(location=SLAvTfits$W_treatment,yvar=SLAvTfits$SLAref,se=SLAvTfits$SLAref.CI)                    
table2[17:18,5] <- mktable(location=SLAvTfits$W_treatment,yvar=SLAvTfits$theta,se=SLAvTfits$theta.CI)                    


write.csv(table2,file="output/Table1.csv",row.names=F)

