#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
#-------------------------------------------------------

#- get data for campaign 1 

#avt.1<-subset(avt.l,avt.l$campaign==1)

avt.l<-getAQ()

avt.l.list <- subset(avt.l, avt.l$campaign==1 & avt.l$LightFac %in% c(4) & avt.l$Prov=="B") 



#--plot stomatal conductance
dat3 <- summaryBy(Photo+Cond+Tair+Tleaf~Room+W_treatment,FUN=c(mean,standard.error),data=subset(avt.l.list,avt.l.list$Cond<2.5),na.rm=T)

pdf(file="output/Figure6-gs_vs_Temperature.pdf",width=3.5,height=3)
# png(file="output/Drought_Plants/Figure1-Photo_vs_Temperature.png",width=300,height=300)
par(mar=c(3.5,4,0.5,0.5),oma=c(0,0,0,0))

smoothplot(Tleaf, Cond, W_treatment,kgam=6,linecols=c(alpha(COL[1],1),alpha(COL[2],1)),
           #polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3),alpha(COL[3],0.3)),
           polycolor=c(alpha(COL[1],0.3),alpha(COL[2],0.3)),
           linecol=c("black","red"),pointcols=NA,
           cex=1,main="",
           xlim=c(18,42),ylim=c(0,2),xlab="",ylab="",
           data=subset(avt.l.list, avt.l.list$Cond<2), axes=F)

plotBy(Cond.mean~Tleaf.mean|W_treatment,data=dat3,las=1,ylim=c(0,2),legend=F,pch=16,cex=0.5,col=COL,add=T,
       panel.first=adderrorbars(x=dat3$Tleaf.mean,
                                y=dat3$Cond.mean,
                                SE=dat3$Cond.standard.error,direction="updown"),
       panel.first=adderrorbars(x=dat3$Tleaf.mean,y=dat3$Cond.mean,SE=dat3$Tleaf.standard.error,direction="leftright"),
       
       
       axes=F,xlab="Tair",cex.lab=2)
palette(COL) 
points(Cond.mean~Tleaf.mean,data=dat3,add=T,pch=21,cex=1.2,legend=F,col="black",bg=W_treatment)


magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2)
title(xlab=expression(Measurement~T[leaf]~(degree*C)),cex.lab=1.3,line=2)
title(ylab=expression(g[s]~(mmol~m^-2~s^-1)),cex.lab=1.3,line=2)

dat_w<-subset(dat3,dat3$Room %in% c(4,5))
segments(dat_w$Tleaf.mean[1], dat_w$Cond.mean[1], x1 = dat_w$Tleaf.mean[3], y1 =dat_w$Cond.mean[3],lty=2,"black",lwd=2)
segments(dat_w$Tleaf.mean[1], dat_w$Cond.mean[1], x1 = dat_w$Tleaf.mean[4], y1 =dat_w$Cond.mean[4],lty=2,"black",lwd=2)

dev.off()

